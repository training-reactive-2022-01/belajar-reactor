package com.muhardin.endy.belajar.reactor;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class HaloReactor {
    public static void main(String[] args) {
        System.out.println("Halo Reactor");

        System.out.println("Membuat Mono");
        Mono<String> nama = Mono.just("Endy");

        System.out.println("Membaca isi Mono");
        System.out.println("Isi Mono : "+nama.block());

        System.out.println("Memproses mono");
        nama.map(s -> {
            System.out.println("Panjang nama : "+s.length());
            return s;
        })
        .map(x -> x.substring(0, 1))
        .map(y -> {
            System.out.println(y.toUpperCase());
            return y;
        }).block();


        System.out.println("Membuat Flux");
        Flux<String> daftarEmail = Flux.fromArray(
            new String[]{"endy@artivisi.com", "endy.muhardin@gmail.com"});

        System.out.println("Memproses isi flux");
        daftarEmail.doOnNext(s -> System.out.println(s))
        .then().block();
    }
}
